package main

import (
	"flag"
	"log"
	"net/http"

	"gitlab.some.systems/b/api/client"
	"gitlab.some.systems/o/taginspector/inspector"
	"gitlab.some.systems/o/taginspector/provider"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/viper"
)

var (
	configPath = flag.String("config", ".", "config path")

	version     string
	startMetric = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "start",
			Help: "start service notify",
		},
		[]string{"service", "version"},
	)
)

func init() {
	prometheus.MustRegister(startMetric)
}

func readInConfig() error {
	flag.Parse()
	viper.SetConfigName("config")
	viper.AddConfigPath(*configPath)

	viper.SetDefault("prometheus", ":9092")

	viper.SetDefault("cert", "some.systems.cer")
	viper.SetDefault("key", "some.systems.key")

	// API
	viper.SetDefault("api_host", "ortb_api.some.systems:10500")
	viper.SetDefault("api_cert", "api.cer")
	viper.SetDefault("site", "ortb_api.some.systems")
	viper.SetDefault("admin_key", "SOMEKEYFORAPI")

	viper.SetDefault("api_context_timeout", 10)
	viper.SetDefault("tmt_context_timeout", 60)
	viper.SetDefault("tmt_request_timeout", 60)
	viper.SetDefault("inspector_timeout", 2)
	viper.SetDefault("prefix", "gobid")

	viper.AutomaticEnv()
	return viper.ReadInConfig()
}

func main() {
	startMetric.WithLabelValues("taginspector", version).SetToCurrentTime()

	if err := readInConfig(); err != nil {
		log.Fatal("config read: ", err)
	}

	apiConn, apiClient := client.NewClientNode(
		viper.GetString("api_host"),
		viper.GetString("api_cert"),
		viper.GetString("site"),
		client.AdminCred{Key: viper.GetString("admin_key")},
	)
	defer apiConn.Close()

	go inspector.Inspector(
		apiClient,
		viper.GetInt64("api_context_timeout"),
		viper.GetInt64("tmt_context_timeout"),
		viper.GetInt64("tmt_request_timeout"),
		viper.GetInt64("inspector_timeout"),
		provider.NewPrefix(viper.GetString("prefix")),
	)

	http.ListenAndServe(viper.GetString("prometheus"), promhttp.Handler())
}
