package provider

import (
	"bytes"
	"io/ioutil"
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/suite"
)

type CommonSuite struct {
	suite.Suite

	provider Common
	name     string
	prefix   Prefix

	registry *prometheus.Registry
}

func (s *CommonSuite) SetupTest() {
	s.prefix = NewPrefix("common_prefix")
	s.name = "common_name"
	s.provider = NewCommon(s.name, s.prefix)

	s.registry = prometheus.NewPedanticRegistry()
	ProviderMetric.Reset()
	s.registry.MustRegister(ProviderMetric)
}

func (s *CommonSuite) TestName() {
	s.Equal(s.name, s.provider.Name())
}

func (s *CommonSuite) TestPrefix() {
	s.Equal(s.prefix, s.provider.Prefix())
}

func (s *CommonSuite) TestDecodeResponse() {
	for _, tt := range []struct {
		body  string
		isErr bool
	}{
		{
			body:  `{`,
			isErr: true,
		},
		{
			body: `{"count":0,"results":[]}`,
		},
	} {
		var data interface{}
		err := s.provider.DecodeResponse(
			ioutil.NopCloser(bytes.NewReader([]byte(tt.body))), &data,
		)
		if tt.isErr {
			s.Error(err)
		} else {
			s.NoError(err)
		}
	}
}

func TestCommonSuite(t *testing.T) {
	suite.Run(t, new(CommonSuite))
}
