package provider

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"

	"gitlab.some.systems/b/api/engine/node/pb"

	"github.com/prometheus/client_golang/prometheus"
)

const (
	IncidentsHours = 168
)

var (
	ProviderMetric = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name: "taginspector_provider",
			Help: "taginspector provider metric; name, key, method, is_error and duration",
		},
		[]string{"name", "key", "method", "is_error"},
	)
)

func init() {
	prometheus.MustRegister(ProviderMetric)
}

type Requester interface {
	Do(*http.Request) (*http.Response, error)
}

type ProviderActions interface {
	Add(client Requester, key string, tags TagList) error
	Remove(client Requester, key string, tags TagList) error
	Pause(client Requester, key string, tags TagList) error
	Search(client Requester, key string) (TagList, error)
	Incidents(client Requester, key string) (TagList, error)
}

type Provider interface {
	Name() string
	Key(*pb.TagInspectorClient) string
	Prefix() Prefix
	NewTag(creativeAdID, creativeName, creativeHTMLContent string) Tag
	ProviderActions
}

type Common struct {
	name   string
	prefix Prefix
}

func NewCommon(name string, prefix Prefix) Common {
	return Common{name: name, prefix: prefix}
}

func (p Common) Name() string {
	return p.name
}

func (p Common) Prefix() Prefix {
	return p.prefix
}

func (p Common) DecodeResponse(body io.Reader, v interface{}) (err error) {
	var data []byte
	if data, err = ioutil.ReadAll(body); err == nil {
		if err = json.Unmarshal(data, v); err != nil {
			log.Printf("%s | DecodeResponse failed: %s", p.Name(), string(data))
		}
	}
	return err
}

func (p Common) ResponseError(action string, response interface{}) error {
	return fmt.Errorf("%s | %s failed: %s", p.name, action, response)
}

func (p Common) Metric(key, method string, err error, t time.Time) {
	ProviderMetric.WithLabelValues(
		p.name, key, method, strconv.FormatBool(err != nil),
	).Observe(float64(time.Since(t)))
}
