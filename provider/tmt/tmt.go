package tmt

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"gitlab.some.systems/b/api/engine/node/pb"
	"gitlab.some.systems/o/taginspector/provider"
)

type Tag struct {
	AdTagID  string `json:"adTagId,omitempty"`
	Name     string `json:"tagName,omitempty"`
	Base64   string `json:"creativeBase64,omitempty"`
	LastScan string `json:"lastScan,omitempty"`
	Pause    string `json:"pause,omitempty"`
}

func NewTag(prefix provider.Prefix, adID, name, htmlContent string) Tag {
	id := prefix.WithString(adID)
	return Tag{
		AdTagID: id,
		Name:    strings.TrimSpace(id + " " + name),
		Base64:  base64.StdEncoding.EncodeToString([]byte(htmlContent)),
	}
}

func (t Tag) ID() string {
	return t.AdTagID
}

func (t Tag) ExtID() string {
	return t.AdTagID
}

func (t Tag) Scan() string {
	return t.LastScan
}

func (t Tag) IsActive() bool {
	return t.Pause == "f"
}

func (t Tag) IsValid() bool {
	// deleted tags can be with pause == ""
	return t.Pause != ""
}

func (t Tag) IsEqual(tag provider.Tag) (equal bool) {
	if tag, ok := tag.(Tag); ok {
		equal = t.AdTagID == tag.AdTagID && t.Name == tag.Name && t.Base64 == tag.Base64
	}
	return equal
}

func query(key, action string, values url.Values) string {
	values.Set("key", key)
	values.Set("action", action)
	return "https://www.themediatrust.com/api/?" + values.Encode()
}

type TMT struct {
	provider.Common
}

func NewTMT(prefix provider.Prefix) provider.Provider {
	return TMT{Common: provider.NewCommon("tmt", prefix)}
}

func (p TMT) Key(client *pb.TagInspectorClient) string {
	return client.TmtKey
}

func (p TMT) NewTag(adID, name, htmlContent string) provider.Tag {
	return NewTag(p.Prefix(), adID, name, htmlContent)
}

type AddRequest struct {
	Tags []Tag `json:"enabled_tags,omitempty"`
}

func NewAddRequest(tagList provider.TagList) AddRequest {
	tags := []Tag{}
	for _, t := range tagList {
		if t, ok := t.(Tag); ok {
			tags = append(tags, t)
		}
	}
	return AddRequest{Tags: tags}
}

type AddResponse map[string]interface{}

func (p TMT) Add(client provider.Requester, key string, tags provider.TagList) (err error) {
	t := time.Now()
	action := "JSON_tag_import"

	b := &bytes.Buffer{}
	if err = json.NewEncoder(b).Encode(NewAddRequest(tags)); err == nil {
		var request *http.Request
		if request, err = http.NewRequest(
			"POST",
			query(key, action, url.Values{}),
			b,
		); err == nil {
			request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
			var r *http.Response
			if r, err = client.Do(request); err == nil {
				response := AddResponse{}
				if err = p.DecodeResponse(r.Body, &response); err == nil {
					if len(response) != len(tags) {
						err = p.ResponseError(action, response)
					}
				}
				defer r.Body.Close()
			}
		}
	}
	p.Metric(key, action, err, t)

	return err
}

type UpdateResponse struct {
	Failed []string `json:"FAILED,omitempty"`
}

func (p TMT) update(client provider.Requester, key, action, tags string) (err error) {
	t := time.Now()

	var request *http.Request
	if request, err = http.NewRequest(
		"GET",
		query(key, action, url.Values{"adTagIds": []string{tags}}),
		nil,
	); err == nil {
		var r *http.Response
		if r, err = client.Do(request); err == nil {
			response := UpdateResponse{}
			if err = p.DecodeResponse(r.Body, &response); err == nil {
				if len(response.Failed) != 0 {
					err = p.ResponseError(action, strings.Join(response.Failed, ","))
				}
			}
			defer r.Body.Close()
		}
	}
	p.Metric(key, action, err, t)

	return err
}

func (p TMT) Pause(client provider.Requester, key string, tags provider.TagList) (err error) {
	return p.update(client, key, "tag_pause", tags.String())
}

func (p TMT) Remove(client provider.Requester, key string, tags provider.TagList) (err error) {
	return p.update(client, key, "tag_remove", tags.String())
}

func tagList(tags []Tag) (l provider.TagList) {
	for _, t := range tags {
		l = append(l, t)
	}
	return l
}

type SearchResponse struct {
	Results []Tag `json:"results,omitempty"`
}

func (p TMT) Search(client provider.Requester, key string) (tags provider.TagList, err error) {
	t := time.Now()
	action := "search_tags"

	var request *http.Request
	if request, err = http.NewRequest(
		"GET",
		query(key, action, url.Values{
			"ad_string": []string{p.Prefix().String()},
			"verbose":   []string{"t"},
		}),
		nil,
	); err == nil {
		var r *http.Response
		if r, err = client.Do(request); err == nil {
			response := SearchResponse{}
			if err = p.DecodeResponse(r.Body, &response); err == nil {
				tags = tagList(response.Results).Valid(p.Prefix())
			}
			defer r.Body.Close()
		}
	}
	p.Metric(key, action, err, t)

	return tags, err
}

type IncidentsResponse struct {
	Incidents []Tag `json:"incidents,omitempty"`
}

func (p TMT) Incidents(client provider.Requester, key string) (tags provider.TagList, err error) {
	t := time.Now()
	action := "tag_incident_summary"

	var request *http.Request
	if request, err = http.NewRequest(
		"GET",
		query(key, action, url.Values{
			"hours":          []string{strconv.Itoa(provider.IncidentsHours)},
			"showTagDetails": []string{"t"},
		}),
		nil,
	); err == nil {
		var r *http.Response
		if r, err = client.Do(request); err == nil {
			response := IncidentsResponse{}
			if err = p.DecodeResponse(r.Body, &response); err == nil {
				tags = tagList(response.Incidents).Valid(p.Prefix())
			}
			defer r.Body.Close()
		}
	}
	p.Metric(key, action, err, t)

	return tags, err
}
