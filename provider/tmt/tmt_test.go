package tmt

import (
	"bytes"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"

	"gitlab.some.systems/b/api/engine/node/pb"
	"gitlab.some.systems/o/taginspector/provider"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

var (
	errStubHttpClient = errors.New("errStubHttpClient")
	errUnmarshalJSON  = errors.New("invalid character 'E' looking for beginning of value")
)

type TMTSuite struct {
	suite.Suite

	tmt    provider.Provider
	prefix provider.Prefix
	key    string
	tags   provider.TagList

	registry *prometheus.Registry
}

func (s *TMTSuite) SetupTest() {
	s.prefix = provider.NewPrefix("gobid")
	s.tmt = NewTMT(s.prefix)
	s.key = "test_tmt"
	s.tags = provider.TagList{
		Tag{
			AdTagID: s.prefix.String() + "_1.1.1",
			Name:    s.prefix.String() + "_1.1.1 name",
			Base64:  "aHRtbENvbnRlbnQx",
			Pause:   "f",
		},
		NewTag(s.prefix, "1.1.2", "name", "htmlContent2"),
	}

	s.registry = prometheus.NewPedanticRegistry()
	provider.ProviderMetric.Reset()
	s.registry.MustRegister(provider.ProviderMetric)
}

func (s *TMTSuite) TestName() {
	s.Equal("tmt", s.tmt.Name())
}

func (s *TMTSuite) TestKey() {
	s.Equal("tmtKey", s.tmt.Key(&pb.TagInspectorClient{TmtKey: "tmtKey"}))
}

func (s *TMTSuite) TestNewTag() {
	adID := "1.2.3"
	name := "name"
	htmlContent := "htmlContent"
	s.Equal(NewTag(s.prefix, adID, name, htmlContent), s.tmt.NewTag(adID, name, htmlContent))
}

type StubHttpClient struct {
	Request  *http.Request
	response *http.Response
	err      error
}

func (c *StubHttpClient) Do(request *http.Request) (*http.Response, error) {
	c.Request = request
	return c.response, c.err
}

func newHttpClient(body string) *StubHttpClient {
	return &StubHttpClient{response: &http.Response{
		Body:   ioutil.NopCloser(bytes.NewReader([]byte(body))),
		Header: http.Header{"Content-Type": []string{"text/html"}},
	}}
}

func newHttpClientError() *StubHttpClient {
	return &StubHttpClient{
		response: &http.Response{},
		err:      errStubHttpClient,
	}
}

func (s *TMTSuite) testMetric(m map[string]string) {
	g, err := s.registry.Gather()
	s.Require().NoError(err)

	for _, label := range g[0].GetMetric()[0].GetLabel() {
		s.Equal(m[label.GetName()], label.GetValue())
	}
}

func (s *TMTSuite) TestAdd() {
	s.NoError(s.tmt.Add(
		newHttpClient(`{"gobid_1.1.1 name": "Inserted one Record (25a3fdd49000bcff2ab6ee15066867b1)","gobid_1.1.2 name": "Inserted one Record (f847908b6a7330c5fa47d45da25941a7)"}`),
		s.key,
		s.tags,
	))

	s.testMetric(map[string]string{
		"name":     s.tmt.Name(),
		"key":      s.key,
		"method":   "JSON_tag_import",
		"is_error": "false",
	})
}

func (s *TMTSuite) TestAddQuery() {
	client := newHttpClient("{}")
	s.tmt.Add(client, s.key, s.tags)

	s.Equal("POST", client.Request.Method)
	s.Equal(
		query(s.key, "JSON_tag_import", url.Values{}),
		client.Request.URL.String(),
	)
	s.Equal("application/x-www-form-urlencoded", client.Request.Header.Get("Content-Type"))
}

func (s *TMTSuite) TestAddError() {
	for _, tt := range []struct {
		httpClient provider.Requester
		err        error
	}{
		{
			httpClient: newHttpClientError(),
			err:        errStubHttpClient,
		},
		{
			httpClient: newHttpClient(`{"gobid_1.1.1 name": "Inserted one Record (25a3fdd49000bcff2ab6ee15066867b1)"}`),
			err:        errors.New("tmt | JSON_tag_import failed: map[gobid_1.1.1 name:Inserted one Record (25a3fdd49000bcff2ab6ee15066867b1)]"),
		},
		{
			httpClient: newHttpClient("Error - Connection rate of 1 per 60 second(s) exceeded."),
			err:        errUnmarshalJSON,
		},
	} {
		err := s.tmt.Add(tt.httpClient, s.key, s.tags)
		s.Error(err)
		s.Equal(tt.err.Error(), err.Error())

		s.testMetric(map[string]string{
			"name":     s.tmt.Name(),
			"key":      s.key,
			"method":   "JSON_tag_import",
			"is_error": "true",
		})
	}
}

func (s *TMTSuite) TestPause() {
	s.NoError(s.tmt.Pause(
		newHttpClient(`{"SUCCESS": ["gobid_1.1.1","gobid_1.1.2"]}`),
		s.key,
		s.tags,
	))

	s.testMetric(map[string]string{
		"name":     s.tmt.Name(),
		"key":      s.key,
		"method":   "tag_pause",
		"is_error": "false",
	})
}

func (s *TMTSuite) TestPauseError() {
	for _, tt := range []struct {
		httpClient provider.Requester
		err        error
	}{
		{
			httpClient: newHttpClientError(),
			err:        errStubHttpClient,
		},
		{
			httpClient: newHttpClient(`{"FAILED": ["gobid_1.1.1","gobid_1.1.2"]}`),
			err:        errors.New("tmt | tag_pause failed: gobid_1.1.1,gobid_1.1.2"),
		},
		{
			httpClient: newHttpClient("Error - Connection rate of 1 per 60 second(s) exceeded."),
			err:        errUnmarshalJSON,
		},
	} {
		err := s.tmt.Pause(tt.httpClient, s.key, s.tags)
		s.Error(err)
		s.Equal(tt.err.Error(), err.Error())

		s.testMetric(map[string]string{
			"name":     s.tmt.Name(),
			"key":      s.key,
			"method":   "tag_pause",
			"is_error": "true",
		})
	}
}

func (s *TMTSuite) TestPauseQuery() {
	client := newHttpClient("{}")
	s.tmt.Pause(client, s.key, s.tags)

	s.Equal("GET", client.Request.Method)
	s.Equal(
		query(s.key, "tag_pause", url.Values{
			"adTagIds": []string{"gobid_1.1.1,gobid_1.1.2"},
		}),
		client.Request.URL.String(),
	)
}

func (s *TMTSuite) TestRemoveQuery() {
	client := newHttpClient("{}")
	s.tmt.Remove(client, s.key, s.tags)

	s.Equal("GET", client.Request.Method)
	s.Equal(
		query(s.key, "tag_remove", url.Values{
			"adTagIds": []string{"gobid_1.1.1,gobid_1.1.2"},
		}),
		client.Request.URL.String(),
	)
}

func (s *TMTSuite) TestSearch() {
	for _, tt := range []struct {
		body         string
		tagsExpected provider.TagList
	}{
		{
			body:         `{}`,
			tagsExpected: provider.TagList{},
		},
		{
			body:         `{"count":0,"results":[]}`,
			tagsExpected: provider.TagList{},
		},
		{
			body:         `{"count":1,"results":[{"adTagId":"","creativeBase64":"","tagName":"gobid_1.1.1 name","lastScan":null,"pause":"f"}]}`,
			tagsExpected: provider.TagList{},
		},
		{
			body:         `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"aHRtbENvbnRlbnQx","tagName":"gobid_1.1.1 name","lastScan":null,"pause":"f"}]}`,
			tagsExpected: provider.TagList{s.tags[0]},
		},
	} {
		tags, err := s.tmt.Search(newHttpClient(tt.body), s.key)
		s.Equal(tt.tagsExpected, tags)
		s.NoError(err)

		s.testMetric(map[string]string{
			"name":     s.tmt.Name(),
			"key":      s.key,
			"method":   "search_tags",
			"is_error": "false",
		})
	}
}

func (s *TMTSuite) TestSearchQuery() {
	client := newHttpClient("{}")
	s.tmt.Search(client, s.key)

	s.Equal("GET", client.Request.Method)
	s.Equal(
		"https://www.themediatrust.com/api/?"+url.Values{
			"key":       []string{s.key},
			"action":    []string{"search_tags"},
			"ad_string": []string{"gobid"},
			"verbose":   []string{"t"},
		}.Encode(),
		client.Request.URL.String(),
	)
}

func (s *TMTSuite) TestSearchError() {
	for _, httpClient := range []provider.Requester{
		newHttpClientError(),
		newHttpClient("Error - Connection rate of 1 per 60 second(s) exceeded."),
	} {
		tags, err := s.tmt.Search(httpClient, s.key)
		s.Nil(tags)
		s.Error(err)

		s.testMetric(map[string]string{
			"name":     s.tmt.Name(),
			"key":      s.key,
			"method":   "search_tags",
			"is_error": "true",
		})
	}
}

func (s *TMTSuite) TestIncidents() {
	for _, tt := range []struct {
		body         string
		tagsExpected provider.TagList
	}{
		{
			body:         `{}`,
			tagsExpected: provider.TagList{},
		},
		{
			body:         `{"count":0,"incidents":[]}`,
			tagsExpected: provider.TagList{},
		},
		{
			body:         `{"count":1,"incidents":[{"adTagId":"","tagName":"gobid_1.1.1 name","pause":"f"}]}`,
			tagsExpected: provider.TagList{},
		},
		{
			body:         `{"count":1,"incidents":[{"adTagId":"gobid_1","tagName":"gobid_1 name","pause":"f"}]}`,
			tagsExpected: provider.TagList{},
		},
		{
			body: `{"count":1,"incidents":[{"adTagId":"gobid_1.1.1","tagName":"gobid_1.1.1 name","pause":"f"}]}`,
			tagsExpected: provider.TagList{
				Tag{AdTagID: "gobid_1.1.1", Name: "gobid_1.1.1 name", Pause: "f"},
			},
		},
	} {
		tags, err := s.tmt.Incidents(newHttpClient(tt.body), s.key)
		s.Equal(tt.tagsExpected, tags)
		s.NoError(err)

		s.testMetric(map[string]string{
			"name":     s.tmt.Name(),
			"key":      s.key,
			"method":   "tag_incident_summary",
			"is_error": "false",
		})
	}
}

func (s *TMTSuite) TestIncidentsQuery() {
	client := newHttpClient("{}")
	s.tmt.Incidents(client, s.key)

	s.Equal("GET", client.Request.Method)
	s.Equal(
		"https://www.themediatrust.com/api/?"+url.Values{
			"key":            []string{s.key},
			"action":         []string{"tag_incident_summary"},
			"hours":          []string{"168"},
			"showTagDetails": []string{"t"},
		}.Encode(),
		client.Request.URL.String(),
	)
}

func (s *TMTSuite) TestIncidentsError() {
	for _, httpClient := range []provider.Requester{
		newHttpClientError(),
		newHttpClient("Error - Connection rate of 1 per 60 second(s) exceeded."),
	} {
		tags, err := s.tmt.Incidents(httpClient, s.key)
		s.Nil(tags)
		s.Error(err)

		s.testMetric(map[string]string{
			"name":     s.tmt.Name(),
			"key":      s.key,
			"method":   "tag_incident_summary",
			"is_error": "true",
		})
	}
}

func TestTMTSuite(t *testing.T) {
	suite.Run(t, new(TMTSuite))
}

func TestTagExtID(t *testing.T) {
	assert.Equal(t, "1.2.3", Tag{AdTagID: "1.2.3"}.ExtID())
}

func TestTagScan(t *testing.T) {
	assert.Equal(t, "123", Tag{LastScan: "123"}.Scan())
}

func TestTagIsActive(t *testing.T) {
	for _, tt := range []struct {
		pause  string
		result bool
	}{
		{},
		{
			pause: "0",
		},
		{
			pause: "t",
		},
		{
			pause:  "f",
			result: true,
		},
	} {
		assert.Equal(t, tt.result, Tag{Pause: tt.pause}.IsActive())
	}
}

func TestTagIsValid(t *testing.T) {
	for _, tt := range []struct {
		pause  string
		result bool
	}{
		{},
		{
			pause:  "123",
			result: true,
		},
	} {
		assert.Equal(t, tt.result, Tag{Pause: tt.pause}.IsValid())
	}
}

func TestTagIsEqual(t *testing.T) {
	tag := Tag{AdTagID: "adTagID", Name: "name", Base64: "base64"}
	for _, tt := range []struct {
		adTagID string
		name    string
		base64  string
		result  bool
	}{
		{},
		{
			adTagID: "adTagID",
		},
		{
			adTagID: "adTagID",
			name:    "name",
		},
		{
			adTagID: "adTagID",
			name:    "name",
			base64:  "base64",
			result:  true,
		},
	} {
		assert.Equal(
			t,
			tt.result,
			tag.IsEqual(Tag{AdTagID: tt.adTagID, Name: tt.name, Base64: tt.base64}),
		)
	}
}
