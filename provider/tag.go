package provider

import (
	"regexp"
	"strings"
)

type Prefix struct {
	prefix string
	re     *regexp.Regexp
}

func NewPrefix(prefix string) Prefix {
	return Prefix{
		prefix: prefix,
		re:     regexp.MustCompile(`^` + prefix + `_(\d+\.){2}\d+$`),
	}
}

func (p Prefix) String() string {
	return p.prefix
}

func (p Prefix) WithString(s string) string {
	return p.prefix + "_" + s
}

func (p Prefix) Clear(s string) string {
	return strings.Replace(s, p.WithString(""), "", -1)
}

func (p Prefix) Check(s string) bool {
	return p.re.MatchString(s)
}

type Tag interface {
	ID() string
	ExtID() string
	Scan() string
	IsActive() bool
	IsValid() bool
	IsEqual(Tag) bool
}

type TagList []Tag

func (l TagList) Contains(tag Tag) bool {
	for _, t := range l {
		if tag.IsEqual(t) {
			return true
		}
	}
	return false
}

func (l TagList) Exclude(excludeTags TagList) TagList {
	tags := TagList{}
	for _, t := range l {
		if !excludeTags.Contains(t) {
			tags = append(tags, t)
		}
	}
	return tags
}

func (l TagList) List() []string {
	ids := []string{}
	for _, t := range l {
		ids = append(ids, t.ID())
	}
	return ids
}

func (l TagList) String() string {
	return strings.Join(l.List(), ",")
}

func (l TagList) Range(includeFunc func(t Tag) bool) TagList {
	tags := TagList{}
	for _, t := range l {
		if includeFunc(t) {
			tags = append(tags, t)
		}
	}
	return tags
}

func (l TagList) Active() TagList {
	return l.Range(func(t Tag) bool {
		return t.IsActive()
	})
}

func (l TagList) Valid(p Prefix) TagList {
	return l.Range(func(t Tag) bool {
		return p.Check(t.ExtID()) && t.IsValid()
	})
}
