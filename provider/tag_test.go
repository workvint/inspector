package provider

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrefix(t *testing.T) {
	p := NewPrefix("test")
	assert.Equal(t, "test", p.String())
	assert.Equal(t, "test_a123", p.WithString("a123"))
	assert.Equal(t, "a123", p.Clear(p.WithString("a123")))
}

func TestPrefixCheck(t *testing.T) {
	p := NewPrefix("test")
	for _, tt := range []struct {
		s      string
		result bool
	}{
		{},
		{
			s: "test",
		},
		{
			s: "1test",
		},
		{
			s: "test1",
		},
		{
			s: "test_",
		},
		{
			s: "test_1",
		},
		{
			s: "test_1.2",
		},
		{
			s: "test_1.2.",
		},
		{
			s:      "test_1.2.3",
			result: true,
		},
		{
			s:      "test_11.222.3333",
			result: true,
		},
		{
			s: "test_11.222.3333.",
		},
		{
			s: "test_11.222.3333.4",
		},
	} {
		assert.Equal(t, tt.result, p.Check(tt.s))
	}
}

type StubTag struct {
	AdID     string `json:"adId,omitempty"`
	LastScan string `json:"lastScan,omitempty"`
	Pause    string `json:"pause,omitempty"`
}

func (t StubTag) ID() string {
	return t.AdID
}

func (t StubTag) ExtID() string {
	return t.AdID
}

func (t StubTag) Scan() string {
	return t.LastScan
}

func (t StubTag) IsActive() bool {
	return t.Pause == "0"
}

func (t StubTag) IsEqual(tag Tag) (equal bool) {
	if tag, ok := tag.(StubTag); ok {
		equal = t.AdID == tag.AdID
	}
	return equal
}

func (t StubTag) IsValid() bool {
	return true
}

func TestTagListContains(t *testing.T) {
	for _, tt := range []struct {
		tagList TagList
		tag     StubTag
		result  bool
	}{
		{
			tagList: TagList{},
			tag:     StubTag{},
		},
		{
			tagList: TagList{StubTag{AdID: "id"}},
			tag:     StubTag{},
		},
		{
			tagList: TagList{StubTag{AdID: "id"}},
			tag:     StubTag{AdID: "id"},
			result:  true,
		},
	} {
		assert.Equal(t, tt.result, tt.tagList.Contains(tt.tag))
	}
}

func TestTagListExclude(t *testing.T) {
	for _, tt := range []struct {
		tagList1 TagList
		tagList2 TagList
		result   TagList
	}{
		{
			tagList1: TagList{},
			tagList2: TagList{},
			result:   TagList{},
		},
		{
			tagList1: TagList{},
			tagList2: TagList{StubTag{AdID: "id"}},
			result:   TagList{},
		},
		{
			tagList1: TagList{StubTag{AdID: "id"}},
			tagList2: TagList{},
			result:   TagList{StubTag{AdID: "id"}},
		},
		{
			tagList1: TagList{StubTag{AdID: "id"}},
			tagList2: TagList{StubTag{AdID: "id"}},
			result:   TagList{},
		},
		{
			tagList1: TagList{StubTag{AdID: "id_1"}},
			tagList2: TagList{StubTag{AdID: "id_2"}},
			result:   TagList{StubTag{AdID: "id_1"}},
		},
	} {
		assert.Equal(t, tt.result, tt.tagList1.Exclude(tt.tagList2))
	}
}

func TestTagListList(t *testing.T) {
	for _, tt := range []struct {
		tagList TagList
		result  []string
	}{
		{
			tagList: TagList{},
			result:  []string{},
		},
		{
			tagList: TagList{StubTag{AdID: "id1"}, StubTag{AdID: "id2"}},
			result:  []string{"id1", "id2"},
		},
	} {
		assert.Equal(t, tt.result, tt.tagList.List())
	}
}

func TestTagListString(t *testing.T) {
	for _, tt := range []struct {
		tagList TagList
		result  string
	}{
		{
			tagList: TagList{},
			result:  "",
		},
		{
			tagList: TagList{StubTag{AdID: "id1"}, StubTag{AdID: "id2"}},
			result:  "id1,id2",
		},
	} {
		assert.Equal(t, tt.result, tt.tagList.String())
	}
}

func TestTagListActive(t *testing.T) {
	for _, tt := range []struct {
		tagList TagList
		result  TagList
	}{
		{
			tagList: TagList{},
			result:  TagList{},
		},
		{
			tagList: TagList{StubTag{AdID: "id1"}, StubTag{AdID: "id2", Pause: "0"}},
			result:  TagList{StubTag{AdID: "id2", Pause: "0"}},
		},
	} {
		assert.Equal(t, tt.result, tt.tagList.Active())
	}
}

func TestTagValid(t *testing.T) {
	for _, tt := range []struct {
		tagList TagList
		result  TagList
	}{
		{
			tagList: TagList{},
			result:  TagList{},
		},
		{
			tagList: TagList{StubTag{AdID: "gobid_1.1"}},
			result:  TagList{},
		},
		{
			tagList: TagList{StubTag{}, StubTag{AdID: "gobid_1.1.2"}},
			result:  TagList{StubTag{AdID: "gobid_1.1.2"}},
		},
	} {
		assert.Equal(t, tt.result, tt.tagList.Valid(NewPrefix("gobid")))
	}
}
