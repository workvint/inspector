package geoedge

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.some.systems/b/api/engine/node/pb"
	"gitlab.some.systems/o/taginspector/provider"
)

const (
	rate = time.Second / 5
)

type Project struct {
	TagID         string `json:"id,omitempty"`
	Tag           string `json:"tag,omitempty"`
	Name          string `json:"name,omitempty"`
	AutoScan      int64  `json:"auto_scan,omitempty"`
	ScanType      int64  `json:"scan_type,omitempty"`
	ExtLineitemID string `json:"ext_lineitem_id,omitempty"`
	// string or number
	LastRunTime interface{} `json:"last_run_time,omitempty"`
	// string or map
	Locations interface{} `json:"locations,omitempty"`
}

func NewProject(prefix provider.Prefix, adID, name, htmlContent string) Project {
	id := prefix.WithString(adID)
	return Project{
		Tag:           htmlContent,
		Name:          strings.TrimSpace(id + " " + name),
		AutoScan:      1,
		ScanType:      1,
		ExtLineitemID: id,
		Locations:     "US",
	}
}

func (p Project) ID() string {
	return p.TagID
}

func (p Project) ExtID() string {
	return p.ExtLineitemID
}

func (p Project) Scan() (s string) {
	if p.LastRunTime != nil {
		s = fmt.Sprintf("%v", p.LastRunTime)
	}
	return s
}

func (p Project) IsActive() bool {
	return p.AutoScan == 1
}

func (p Project) IsValid() bool {
	return true
}

func (p Project) IsEqual(tag provider.Tag) (equal bool) {
	if tag, ok := tag.(Project); ok {
		equal = p.ExtLineitemID == tag.ExtLineitemID &&
			p.Name == tag.Name && p.Tag == tag.Tag
	}
	return equal
}

type ProjectList []Project

func NewProjectsList(tags provider.TagList) (l ProjectList) {
	for _, t := range tags {
		l = append(l, t.(Project))
	}
	return l
}

func (l ProjectList) TagList() (tags provider.TagList) {
	for _, t := range l {
		tags = append(tags, t)
	}
	return tags
}

type GeoEdge struct {
	provider.Common
}

func NewGeoEdge(prefix provider.Prefix) provider.Provider {
	return GeoEdge{Common: provider.NewCommon("geoedge", prefix)}
}

func (p GeoEdge) Key(client *pb.TagInspectorClient) string {
	return client.GeoedgeKey
}

func (p GeoEdge) NewTag(adID, name, htmlContent string) provider.Tag {
	return NewProject(p.Prefix(), adID, name, htmlContent)
}

type ResponseStatus struct {
	Code    string `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

type Response struct {
	Status ResponseStatus `json:"status,omitempty"`
}

func (r Response) GetStatus() ResponseStatus {
	return r.Status
}

type Responser interface {
	GetStatus() ResponseStatus
}

func newRequest(key, method, action string, body io.Reader) (r *http.Request, err error) {
	if r, err = http.NewRequest(
		method,
		"https://api.geoedge.com/rest/analytics/v3/"+action,
		body,
	); err == nil {
		r.Header.Set("Authorization", key)
	}
	return r, err
}

func (p GeoEdge) decodeResponseSuccess(body io.Reader, r Responser, action string) (err error) {
	if err = p.DecodeResponse(body, &r); err == nil {
		if s := r.GetStatus(); s.Code != "Success" {
			err = p.ResponseError(action, fmt.Sprintf("code: %s; message: %s", s.Code, s.Message))
		}
	}
	return err
}

func (p GeoEdge) Add(client provider.Requester, key string, tags provider.TagList) (err error) {
	t := time.Now()
	action := "add"

	b := &bytes.Buffer{}
	if err = json.NewEncoder(b).Encode(NewProjectsList(tags)); err == nil {
		var request *http.Request
		if request, err = newRequest(
			key, "POST", "projects/bulk", b,
		); err == nil {
			request.Header.Set("Content-Type", "application/json")
			var r *http.Response
			if r, err = client.Do(request); err == nil {
				response := Response{}
				err = p.decodeResponseSuccess(r.Body, &response, action)
				defer r.Body.Close()
			}
		}
	}
	p.Metric(key, action, err, t)

	return err
}

func (p GeoEdge) Remove(client provider.Requester, key string, tags provider.TagList) (err error) {
	t := time.Now()
	action := "remove"

	var request *http.Request
	if request, err = newRequest(
		key, "DELETE", "projects/"+tags.String(), nil,
	); err == nil {
		var r *http.Response
		if r, err = client.Do(request); err == nil {
			response := Response{}
			err = p.decodeResponseSuccess(r.Body, &response, action)
			defer r.Body.Close()
		}
	}
	p.Metric(key, action, err, t)

	return err
}

func (p GeoEdge) iterate(
	projectList ProjectList, tagFunc func(provider.Tag) (Project, error),
) (tags provider.TagList, err error) {
	slow := time.NewTicker(rate)
	for _, t := range projectList.TagList() {
		<-slow.C
		var tag Project
		if tag, err = tagFunc(t); err == nil {
			tags = append(tags, tag)
		} else {
			break
		}

	}
	slow.Stop()
	return tags, err
}

func (p GeoEdge) update(client provider.Requester, key, id string, values url.Values) (err error) {
	t := time.Now()
	action := "update"

	var request *http.Request
	if request, err = newRequest(
		key, "PUT", "projects/"+id, strings.NewReader(values.Encode()),
	); err == nil {
		request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		var r *http.Response
		if r, err = client.Do(request); err == nil {
			response := SearchResponse{}
			err = p.decodeResponseSuccess(r.Body, &response, action)
			defer r.Body.Close()
		}
	}
	p.Metric(key, action, err, t)

	return err
}

func (p GeoEdge) Pause(client provider.Requester, key string, tags provider.TagList) (err error) {
	t := time.Now()
	action := "pause"

	_, err = p.iterate(NewProjectsList(tags), func(t provider.Tag) (Project, error) {
		return Project{}, p.update(client, key, t.ID(), url.Values{"auto_scan": []string{"0"}})
	})
	p.Metric(key, action, err, t)

	return err
}

type DetailsProjectTag struct {
	Tag string `json:"tag,omitempty"`
}

type DetailsProject struct {
	Project
	Tags map[string]DetailsProjectTag `json:"tags,omitempty"`
}

type DetailsDataResponse struct {
	Project DetailsProject `json:"project,omitempty"`
}

type DetailsResponse struct {
	Response
	Data DetailsDataResponse `json:"response,omitempty"`
}

func (p GeoEdge) detailsTag(project DetailsProject) (tag Project, err error) {
	if len(project.Tags) != 1 {
		err = p.ResponseError("decode detailsTag", project.Tags)
	} else {
		tag = project.Project
		for _, t := range project.Tags {
			tag.Tag = strings.Replace(html.UnescapeString(t.Tag), `\/`, `/`, -1)
		}
	}
	return tag, err
}

func (p GeoEdge) details(client provider.Requester, key, id string) (tag Project, err error) {
	t := time.Now()
	action := "details"

	var request *http.Request
	if request, err = newRequest(key, "GET", "projects/"+id, nil); err == nil {
		var r *http.Response
		if r, err = client.Do(request); err == nil {
			response := DetailsResponse{}
			if err = p.decodeResponseSuccess(r.Body, &response, action); err == nil {
				tag, err = p.detailsTag(response.Data.Project)
			}
			defer r.Body.Close()
		}
	}
	p.Metric(key, action, err, t)

	return tag, err
}

type SearchDataResponse struct {
	Projects []Project `json:"projects,omitempty"`
}

type SearchResponse struct {
	Response
	Data SearchDataResponse `json:"response,omitempty"`
}

func (p GeoEdge) Search(client provider.Requester, key string) (tags provider.TagList, err error) {
	t := time.Now()
	action := "search"

	var request *http.Request
	if request, err = newRequest(key, "GET", "projects", nil); err == nil {
		var r *http.Response
		if r, err = client.Do(request); err == nil {
			response := SearchResponse{}
			if err = p.decodeResponseSuccess(r.Body, &response, action); err == nil {
				tags, err = p.iterate(response.Data.Projects, func(t provider.Tag) (Project, error) {
					return p.details(client, key, t.ID())
				})
			}
			defer r.Body.Close()
		}
	}
	p.Metric(key, action, err, t)

	return tags.Valid(p.Prefix()), err
}

type IncidentsAlerts struct {
	ProjectName map[string]string `json:"project_name,omitempty"`
}

type IncidentsDataResponse struct {
	Alerts []IncidentsAlerts `json:"alerts,omitempty"`
}

type IncidentsResponse struct {
	Response
	Data IncidentsDataResponse `json:"response,omitempty"`
}

func incidentsQuery(t time.Time) string {
	return url.Values{"min_datetime": []string{t.Add(-provider.IncidentsHours * time.Hour).Format("2006-01-02 15:04:05")}}.Encode()
}

func incidentsAlertsTags(alerts []IncidentsAlerts) (projects []Project) {
	m := map[string]string{}
	for _, alert := range alerts {
		for id := range alert.ProjectName {
			m[id] = id
		}
	}
	for id := range m {
		projects = append(projects, Project{TagID: id})
	}
	return projects
}

func (p GeoEdge) Incidents(client provider.Requester, key string) (tags provider.TagList, err error) {
	t := time.Now()
	action := "incidents"

	var request *http.Request
	if request, err = newRequest(
		key, "GET", "alerts/history?"+incidentsQuery(t), nil,
	); err == nil {
		var r *http.Response
		if r, err = client.Do(request); err == nil {
			response := IncidentsResponse{}
			if err = p.decodeResponseSuccess(r.Body, &response, action); err == nil {
				tags, err = p.iterate(incidentsAlertsTags(response.Data.Alerts), func(t provider.Tag) (Project, error) {
					return p.details(client, key, t.ID())
				})
			}
			defer r.Body.Close()
		}
	}
	p.Metric(key, action, err, t)

	return tags.Valid(p.Prefix()), err
}
