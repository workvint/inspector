package geoedge

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"
	"time"

	"gitlab.some.systems/b/api/engine/node/pb"
	"gitlab.some.systems/o/taginspector/provider"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

var (
	errStubHttpClient = errors.New("errStubHttpClient")
	errUnmarshalJSON  = errors.New("invalid character 'E' looking for beginning of value")

	responseSuccess = `{"status": {"code": "Success","message": "Success"}}`
)

type GeoEdgeSuite struct {
	suite.Suite

	geoedge GeoEdge
	prefix  provider.Prefix
	key     string
	tags    provider.TagList

	registry *prometheus.Registry
}

func (s *GeoEdgeSuite) SetupTest() {
	s.prefix = provider.NewPrefix("gobid")
	s.geoedge = NewGeoEdge(s.prefix).(GeoEdge)
	s.key = "test_geoedge"
	s.tags = provider.TagList{
		Project{
			ExtLineitemID: s.prefix.String() + "_1.1.1",
			Name:          s.prefix.String() + "_1.1.1 name",
			AutoScan:      1,
			Tag:           "htmlContent",
		},
		NewProject(s.prefix, "1.1.2", "name", "htmlContent2"),
	}

	s.registry = prometheus.NewPedanticRegistry()
	provider.ProviderMetric.Reset()
	s.registry.MustRegister(provider.ProviderMetric)
}

func (s *GeoEdgeSuite) TestName() {
	s.Equal("geoedge", s.geoedge.Name())
}

func (s *GeoEdgeSuite) TestKey() {
	s.Equal("geoedgeKey", s.geoedge.Key(&pb.TagInspectorClient{GeoedgeKey: "geoedgeKey"}))
}

func (s *GeoEdgeSuite) TestNewTag() {
	adID := "1.2.3"
	name := "name"
	htmlContent := "htmlContent"
	s.Equal(
		NewProject(s.prefix, adID, name, htmlContent),
		s.geoedge.NewTag(adID, name, htmlContent),
	)
}

type StubHttpClient struct {
	Request  *http.Request
	response *http.Response
	err      error
}

func (c *StubHttpClient) Do(request *http.Request) (*http.Response, error) {
	c.Request = request
	return c.response, c.err
}

func newHttpClient(body string) *StubHttpClient {
	return &StubHttpClient{response: &http.Response{
		Body:   ioutil.NopCloser(bytes.NewReader([]byte(body))),
		Header: http.Header{"Content-Type": []string{"text/html"}},
	}}
}

func newHttpClientError() *StubHttpClient {
	return &StubHttpClient{
		response: &http.Response{},
		err:      errStubHttpClient,
	}
}

func (s *GeoEdgeSuite) testMetric(m map[string]string) {
	g, err := s.registry.Gather()
	s.Require().NoError(err)

	for _, label := range g[0].GetMetric()[0].GetLabel() {
		s.Equal(m[label.GetName()], label.GetValue())
	}
}

func (s *GeoEdgeSuite) TestAdd() {
	s.NoError(s.geoedge.Add(newHttpClient(responseSuccess), s.key, s.tags))
	s.testMetric(map[string]string{
		"name":     s.geoedge.Name(),
		"key":      s.key,
		"method":   "add",
		"is_error": "false",
	})
}

func (s *GeoEdgeSuite) TestAddQuery() {
	client := newHttpClient("{}")
	s.geoedge.Add(client, s.key, s.tags)

	s.Equal("POST", client.Request.Method)
	s.Equal("application/json", client.Request.Header.Get("Content-Type"))
	s.Equal("https://api.geoedge.com/rest/analytics/v3/projects/bulk", client.Request.URL.String())
}

func (s *GeoEdgeSuite) TestAddError() {
	for _, tt := range []struct {
		httpClient provider.Requester
		err        error
	}{
		{
			httpClient: newHttpClientError(),
			err:        errStubHttpClient,
		},
		{
			httpClient: newHttpClient(`{"status": {"code": "TooManyRequests","message": "Too many requests sent"}}`),
			err:        errors.New("geoedge | add failed: code: TooManyRequests; message: Too many requests sent"),
		},
		{
			httpClient: newHttpClient("Error"),
			err:        errUnmarshalJSON,
		},
	} {
		err := s.geoedge.Add(tt.httpClient, s.key, s.tags)
		s.Error(err)
		s.Equal(tt.err.Error(), err.Error())

		s.testMetric(map[string]string{
			"name":     s.geoedge.Name(),
			"key":      s.key,
			"method":   "add",
			"is_error": "true",
		})
	}
}

func (s *GeoEdgeSuite) TestRemove() {
	s.NoError(s.geoedge.Remove(newHttpClient(responseSuccess), s.key, s.tags))
	s.testMetric(map[string]string{
		"name":     s.geoedge.Name(),
		"key":      s.key,
		"method":   "remove",
		"is_error": "false",
	})
}

func (s *GeoEdgeSuite) TestRemoveQuery() {
	client := newHttpClient("{}")
	s.geoedge.Remove(client, s.key, provider.TagList{
		Project{TagID: "id1"}, Project{TagID: "id2"},
	})

	s.Equal("DELETE", client.Request.Method)
	s.Equal("https://api.geoedge.com/rest/analytics/v3/projects/id1,id2", client.Request.URL.String())
}

func (s *GeoEdgeSuite) TestUpdate() {
	s.NoError(s.geoedge.update(newHttpClient(responseSuccess), s.key, "id1", url.Values{}))
	s.testMetric(map[string]string{
		"name":     s.geoedge.Name(),
		"key":      s.key,
		"method":   "update",
		"is_error": "false",
	})
}

func (s *GeoEdgeSuite) TestUpdateQuery() {
	client := newHttpClient("{}")
	s.geoedge.update(client, s.key, "id1", url.Values{})

	s.Equal("PUT", client.Request.Method)
	s.Equal("application/x-www-form-urlencoded", client.Request.Header.Get("Content-Type"))
	s.Equal("https://api.geoedge.com/rest/analytics/v3/projects/id1", client.Request.URL.String())
}

func (s *GeoEdgeSuite) TestPauseError() {
	for _, tt := range []struct {
		httpClient provider.Requester
		err        error
	}{
		{
			httpClient: newHttpClientError(),
			err:        errStubHttpClient,
		},
		{
			httpClient: newHttpClient("Error"),
			err:        errUnmarshalJSON,
		},
	} {
		err := s.geoedge.Pause(tt.httpClient, s.key, s.tags)
		s.Equal(tt.err.Error(), err.Error())

		s.testMetric(map[string]string{
			"name":     s.geoedge.Name(),
			"key":      s.key,
			"method":   "pause",
			"is_error": "true",
		})
	}
}

func (s *GeoEdgeSuite) TestDetails() {
	project, err := s.geoedge.details(
		newHttpClient(`{
			"status": {"code": "Success","message": "Success"},
			"response": {
				"project": {
					"id": "f4b8a7ceda1a1703e0205e2e1a1d4b16",
					"name": "gobid_1.2.31 Test",
					"ext_lineitem_id": "gobid_1.2.31",
					"auto_scan": 1,
					"scan_type": 1,
					"tags": {
						"TRl1WiGmI6Gw4GDORnxKZw": {
							"tag": "&lt;a href=&quot;https:\/\/goo.gl&quot;&gt;Test Google&lt;\/a&gt;"
						}
					}
				}
			}
		}`),
		s.key,
		"f4b8a7ceda1a1703e0205e2e1a1d4b16",
	)
	s.Equal(Project{
		TagID:         "f4b8a7ceda1a1703e0205e2e1a1d4b16",
		Tag:           "<a href=\"https://goo.gl\">Test Google</a>",
		Name:          "gobid_1.2.31 Test",
		AutoScan:      1,
		ScanType:      1,
		ExtLineitemID: "gobid_1.2.31",
	}, project)
	s.NoError(err)

	s.testMetric(map[string]string{
		"name":     s.geoedge.Name(),
		"key":      s.key,
		"method":   "details",
		"is_error": "false",
	})
}

func (s *GeoEdgeSuite) TestDetailsQuery() {
	client := newHttpClient("{}")
	s.geoedge.details(client, s.key, "id1")

	s.Equal("GET", client.Request.Method)
	s.Equal("https://api.geoedge.com/rest/analytics/v3/projects/id1", client.Request.URL.String())
}

func (s *GeoEdgeSuite) TestDetailsError() {
	for _, tt := range []struct {
		httpClient provider.Requester
		err        error
	}{
		{
			httpClient: newHttpClientError(),
			err:        errStubHttpClient,
		},
		{
			httpClient: newHttpClient(`{"status": {"code": "TooManyRequests","message": "Too many requests sent"}}`),
			err:        errors.New("geoedge | details failed: code: TooManyRequests; message: Too many requests sent"),
		},
		{
			httpClient: newHttpClient("Error"),
			err:        errUnmarshalJSON,
		},
		{
			httpClient: newHttpClient(`{
				"status": {"code": "Success","message": "Success"},
				"response": {
					"project": {
						"id": "f4b8a7ceda1a1703e0205e2e1a1d4b16",
						"tags": {}
					}
				}
			}`),
			err: errors.New("geoedge | decode detailsTag failed: map[]"),
		},
	} {
		_, err := s.geoedge.details(tt.httpClient, s.key, "f4b8a7ceda1a1703e0205e2e1a1d4b16")
		s.Error(err)
		s.Equal(tt.err.Error(), err.Error())

		s.testMetric(map[string]string{
			"name":     s.geoedge.Name(),
			"key":      s.key,
			"method":   "details",
			"is_error": "true",
		})
	}
}

func (s *GeoEdgeSuite) TestSearchQuery() {
	client := newHttpClient("{}")
	s.geoedge.Search(client, s.key)

	s.Equal("GET", client.Request.Method)
	s.Equal("https://api.geoedge.com/rest/analytics/v3/projects", client.Request.URL.String())
}

func (s *GeoEdgeSuite) TestIncidentsQuery() {
	s.Equal("min_datetime=2018-01-23+15%3A10%3A20", incidentsQuery(time.Date(2018, time.January, 30, 15, 10, 20, 0, time.UTC)))

	client := newHttpClient("{}")
	s.geoedge.Incidents(client, s.key)

	s.Equal("GET", client.Request.Method)
	values := client.Request.URL.Query()
	s.Len(values, 1)
	s.NotEmpty(values.Get("min_datetime"))
	s.Equal("/rest/analytics/v3/alerts/history", client.Request.URL.Path)
}

func (s *GeoEdgeSuite) TestIncidentsAlertsTags() {
	response := IncidentsResponse{}
	s.Require().NoError(json.Unmarshal([]byte(`{
	"response": {
		"alerts": [
			{
				"alert_id": "b389fbcbbf3bec78c06ab71750902a9b",
				"alert_name": "Test Alert 1",
				"project_name": {
					"id1": "Test Project 1"
				},
				"tag": {
					"l3jCl0fQmzo": "http:\/\/www.yahoo.com\/"
				}
			},
			{
				"alert_id": "52996960cc18aa70085b36684f81683a",
				"alert_name": "Test Alert 2",
				"project_name": {
					"id1": "Test Project 1",
					"id2": "Test Project 3"
				},
				"tag": {
					"l3jCl0fQmzo": "http:\/\/www.yahoo.com\/"
				}
			}
		]
	}}`), &response))
	s.Equal(
		[]Project{Project{TagID: "id1"}, Project{TagID: "id2"}},
		incidentsAlertsTags(response.Data.Alerts),
	)
}

func TestGeoEdgeSuite(t *testing.T) {
	suite.Run(t, new(GeoEdgeSuite))
}

func TestTagExtID(t *testing.T) {
	assert.Equal(t, "1.2.3", Project{ExtLineitemID: "1.2.3"}.ExtID())
}

func TestTagScan(t *testing.T) {
	for _, tt := range []struct {
		lastRunTime interface{}
		result      string
	}{
		{},
		{
			lastRunTime: "",
		},
		{
			lastRunTime: 123,
			result:      "123",
		},
	} {
		assert.Equal(t, tt.result, Project{LastRunTime: tt.lastRunTime}.Scan())
	}
}

func TestTagIsActive(t *testing.T) {
	for _, tt := range []struct {
		autoScan int64
		result   bool
	}{
		{},
		{
			autoScan: 2,
		},
		{
			autoScan: 1,
			result:   true,
		},
	} {
		assert.Equal(t, tt.result, Project{AutoScan: tt.autoScan}.IsActive())
	}
}

func TestTagIsValid(t *testing.T) {
	assert.True(t, Project{}.IsValid())
}

func TestTagIsEqual(t *testing.T) {
	tag := Project{ExtLineitemID: "extLineitemID", Name: "name", Tag: "tag"}
	for _, tt := range []struct {
		extLineitemID string
		name          string
		tag           string
		result        bool
	}{
		{},
		{
			extLineitemID: "extLineitemID",
		},
		{
			extLineitemID: "extLineitemID",
			name:          "name",
		},
		{
			extLineitemID: "extLineitemID",
			name:          "name",
			tag:           "tag",
			result:        true,
		},
	} {
		assert.Equal(
			t,
			tt.result,
			tag.IsEqual(Project{
				ExtLineitemID: tt.extLineitemID,
				Name:          tt.name,
				Tag:           tt.tag,
			}),
		)
	}
}

func TestProjectList(t *testing.T) {
	tagList := provider.TagList{Project{TagID: "id1"}, Project{TagID: "id2"}}
	assert.Equal(t, tagList, NewProjectsList(tagList).TagList())
}
