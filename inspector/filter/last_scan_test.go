package filter

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLastScanString(t *testing.T) {
	assert.Equal(t, "LastScan", LastScan{}.String())
}

func TestLastScanCheck(t *testing.T) {
	for _, tt := range []struct {
		lastScan string
		err      error
	}{
		{
			err: errLastScan,
		},
		{
			lastScan: "1512348903",
		},
	} {
		assert.Equal(t, tt.err, NewLastScan(tt.lastScan).Check())
	}
}
