package filter

import (
	"errors"
)

var (
	errPause = errors.New("FILTER_PAUSE")
)

type Pause struct {
	active bool
}

func NewPause(active bool) Pause {
	return Pause{active: active}
}

func (p Pause) String() string {
	return "Pause"
}

func (p Pause) Check() error {
	if !p.active {
		return errPause
	}
	return nil
}
