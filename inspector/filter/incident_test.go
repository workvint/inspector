package filter

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIncidentString(t *testing.T) {
	assert.Equal(t, "Incident", Incident{}.String())
}

func TestIncidentCheck(t *testing.T) {
	for _, tt := range []struct {
		tagID     string
		incidents []string
		err       error
	}{
		{},
		{
			tagID:     "id1",
			incidents: []string{},
		},
		{
			tagID:     "id1",
			incidents: []string{"id2", "id3"},
		},
		{
			tagID:     "id1",
			incidents: []string{"id2", "id3", "id1"},
			err:       errIncident,
		},
	} {
		assert.Equal(t, tt.err, NewIncident(tt.tagID, tt.incidents).Check())
	}
}
