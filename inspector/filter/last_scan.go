package filter

import (
	"errors"
)

var (
	errLastScan = errors.New("FILTER_LAST_SCAN")
)

type LastScan struct {
	lastScan string
}

func NewLastScan(lastScan string) LastScan {
	return LastScan{lastScan: lastScan}
}

func (l LastScan) String() string {
	return "LastScan"
}

func (l LastScan) Check() error {
	if l.lastScan == "" {
		return errLastScan
	}
	return nil
}
