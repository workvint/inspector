package filter

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	testErrFilter = errors.New("testErrFilter")
)

type testFilter struct {
	err error
}

func (f testFilter) String() string {
	return "testFilter"
}

func (f testFilter) Check() error {
	return f.err
}

func TestCheck(t *testing.T) {
	for _, tt := range []struct {
		filters []Filterer
		result  error
	}{
		{},
		{
			filters: []Filterer{
				testFilter{},
			},
		},
		{
			filters: []Filterer{
				testFilter{err: testErrFilter},
			},
			result: testErrFilter,
		},
		{
			filters: []Filterer{
				testFilter{},
				testFilter{},
				testFilter{err: testErrFilter},
				testFilter{err: errors.New("testErrFilterFailOther")},
			},
			result: testErrFilter,
		},
	} {
		assert.Equal(t, tt.result, Check(tt.filters))
	}
}
