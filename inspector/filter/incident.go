package filter

import (
	"errors"
)

var (
	errIncident = errors.New("FILTER_INCIDENT")
)

type Incident struct {
	tagID     string
	incidents []string
}

func NewIncident(tagID string, incidents []string) Incident {
	return Incident{
		tagID:     tagID,
		incidents: incidents,
	}
}

func (i Incident) String() string {
	return "Incident"
}

func (i Incident) Check() error {
	for _, id := range i.incidents {
		if i.tagID == id {
			return errIncident
		}
	}
	return nil
}
