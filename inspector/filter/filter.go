package filter

type Filterer interface {
	String() string
	Check() error
}

func Check(filters []Filterer) error {
	for _, filter := range filters {
		if err := filter.Check(); err != nil {
			return err
		}
	}
	return nil
}
