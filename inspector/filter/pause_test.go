package filter

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPauseString(t *testing.T) {
	assert.Equal(t, "Pause", Pause{}.String())
}

func TestPauseCheck(t *testing.T) {
	for _, tt := range []struct {
		active bool
		err    error
	}{
		{
			err: errPause,
		},
		{
			active: true,
		},
	} {
		assert.Equal(t, tt.err, NewPause(tt.active).Check())
	}
}
