package inspector

import (
	"context"
	"log"
	"net"
	"net/http"
	"strconv"
	"time"

	"gitlab.some.systems/b/api/engine/node/pb"
	"gitlab.some.systems/o/taginspector/inspector/filter"
	"gitlab.some.systems/o/taginspector/provider"
	"gitlab.some.systems/o/taginspector/provider/geoedge"
	"gitlab.some.systems/o/taginspector/provider/tmt"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	dataMetric = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name: "taginspector_update",
			Help: "taginspector update metric error and duration",
		},
		[]string{"is_error"},
	)
	filterMetric = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "taginspector_filter",
			Help: "taginspector filter counter metric",
		},
		[]string{"provider", "creative", "error"},
	)
)

func init() {
	prometheus.MustRegister(dataMetric, filterMetric)
}

type MapProviders map[string]provider.Provider

func (m MapProviders) ByClient(c *pb.TagInspectorClient) []provider.Provider {
	providers := []provider.Provider{}
	for _, p := range m {
		if p.Key(c) != "" {
			providers = append(providers, p)
		}
	}
	return providers
}

func NewMapProviders(prefix provider.Prefix) MapProviders {
	m := MapProviders{}
	for _, newFunc := range []func(provider.Prefix) provider.Provider{
		tmt.NewTMT,
		geoedge.NewGeoEdge,
	} {
		p := newFunc(prefix)
		m[p.Name()] = p
	}
	return m
}

type MapClients map[string]*pb.TagInspectorClient
type MapCreatives map[string]string

type MapCreativesByProvider map[string]map[string][]string

func (m MapCreativesByProvider) MapCreatives() MapCreatives {
	mapCreatives := MapCreatives{}
	for _, creativesByKey := range m {
		for _, creatives := range creativesByKey {
			for _, creativeAdID := range creatives {
				mapCreatives[creativeAdID] = creativeAdID
			}
		}
	}
	return mapCreatives
}

func strInList(target string, source []string) bool {
	for _, s := range source {
		if target == s {
			return true
		}
	}
	return false
}

func (m MapCreativesByProvider) Exclude(excludeCreatives []string) MapCreativesByProvider {
	newMap := MapCreativesByProvider{}
	for name, creativesByKey := range m {
		newMap[name] = map[string][]string{}
		for key, creatives := range creativesByKey {
			newCreatives := []string{}
			for _, c := range creatives {
				if !strInList(c, excludeCreatives) {
					newCreatives = append(newCreatives, c)
				}
			}
			newMap[name][key] = newCreatives
		}

	}
	return newMap
}

func data(
	apiClient pb.NodeAPIClient, contextTimeout int64, creatives MapCreatives,
) (m MapClients, err error) {
	t := time.Now()

	ctx, cancel := context.WithTimeout(
		context.Background(), time.Duration(contextTimeout)*time.Second,
	)
	defer cancel()

	var response *pb.TagInspectorResponse
	if response, err = apiClient.TagInspectorCheck(
		ctx, &pb.TagInspectorRequest{Creatives: creatives},
	); err == nil {
		m = response.Clients
	} else {
		log.Println(err.Error())
	}
	dataMetric.WithLabelValues(
		strconv.FormatBool(err != nil),
	).Observe(float64(time.Since(t)))

	return m, err
}

func dataTagsByProvider(
	clients MapClients, providers MapProviders,
) map[string]map[string]provider.TagList {
	m := map[string]map[string]provider.TagList{}
	for _, client := range clients {
		for _, p := range providers.ByClient(client) {
			if name, key := p.Name(), p.Key(client); name != "" && key != "" {
				if _, ok := m[name]; !ok {
					m[name] = map[string]provider.TagList{}
				}
				tags := provider.TagList{}
				for _, c := range client.Creatives {
					tags = append(tags, p.NewTag(c.AdId, c.Name, c.HtmlContent))
				}
				m[name][key] = append(m[name][key], tags...)
			}
		}
	}
	return m
}

func tags(
	storeTags, searchTags, incidentTags provider.TagList,
) (newTags, deleteTags, activeTags, pauseIncidentTags provider.TagList) {
	newTags = storeTags.Exclude(searchTags)
	deleteTags = searchTags.Exclude(storeTags)
	activeTags = searchTags.Exclude(deleteTags)
	pauseIncidentTags = incidentTags.Active()
	return newTags, deleteTags, activeTags, pauseIncidentTags
}

func filters(t provider.Tag, incidentTags provider.TagList) []filter.Filterer {
	return []filter.Filterer{
		filter.NewIncident(t.ID(), incidentTags.List()),
		filter.NewPause(t.IsActive()),
		filter.NewLastScan(t.Scan()),
	}
}

func filterTags(p provider.Provider, activeTags, incidentTags []provider.Tag) ([]string, []string) {
	passCreatives, failCreatives := []string{}, []string{}
	for _, t := range activeTags {
		creativeID := p.Prefix().Clear(t.ExtID())
		if err := filter.Check(filters(t, incidentTags)); err == nil {
			passCreatives = append(passCreatives, creativeID)
		} else {
			failCreatives = append(failCreatives, creativeID)
			filterMetric.WithLabelValues(p.Name(), creativeID, err.Error()).Inc()
		}
	}
	return passCreatives, failCreatives
}

func sleep(timeout int64) {
	time.Sleep(time.Duration(timeout) * time.Second)
}

func inspectKey(
	r provider.Requester,
	requestTimeout int64,
	p provider.Provider,
	key string,
	dataTags provider.TagList,
) ([]string, []string, error) {
	searchTags, err := p.Search(r, key)
	if err != nil {
		return nil, nil, err
	}
	sleep(requestTimeout)

	incidentTags, err := p.Incidents(r, key)
	if err != nil {
		return nil, nil, err
	}
	sleep(requestTimeout)

	newTags, deleteTags, activeTags, pauseIncidentTags := tags(
		dataTags, searchTags, incidentTags,
	)

	if len(pauseIncidentTags) != 0 {
		if err = p.Pause(r, key, pauseIncidentTags); err != nil {
			return nil, nil, err
		}
		sleep(requestTimeout)
	}

	if len(deleteTags) != 0 {
		if err = p.Remove(r, key, deleteTags); err != nil {
			return nil, nil, err
		}
		sleep(requestTimeout)
	}

	if len(newTags) != 0 {
		if err = p.Add(r, key, newTags); err != nil {
			return nil, nil, err
		}
		sleep(requestTimeout)
	}

	passCreatives, failCreatives := filterTags(p, activeTags, incidentTags)
	return passCreatives, failCreatives, err
}

func inspect(
	httpClient provider.Requester,
	requestTimeout int64,
	clients MapClients,
	creativesByProvider MapCreativesByProvider,
	providers MapProviders,
) MapCreativesByProvider {
	m := MapCreativesByProvider{}
	excludeCreatives := []string{}
	for name, tagsByKey := range dataTagsByProvider(clients, providers) {
		if p, ok := providers[name]; ok {
			creativesByKey := map[string][]string{}
			for key, dataTags := range tagsByKey {
				if passCreatives, failCreatives, err := inspectKey(
					httpClient, requestTimeout, p, key, dataTags,
				); err == nil {
					creativesByKey[key] = passCreatives
					excludeCreatives = append(excludeCreatives, failCreatives...)
				} else {
					if netError, ok := err.(net.Error); ok && netError.Timeout() {
						if oldCreatives, ok := creativesByProvider[name][key]; ok {
							creativesByKey[key] = oldCreatives
						}
					}
					log.Println(err.Error())
				}
			}
			m[name] = creativesByKey
		}
	}
	return m.Exclude(excludeCreatives)
}

func Inspector(
	apiClient pb.NodeAPIClient,
	apiContextTimeout, tmtContextTimeout, tmtRequestTimeout, timeout int64,
	prefix provider.Prefix,
) {
	providers := NewMapProviders(prefix)

	clients := MapClients{}
	creativesByProvider := MapCreativesByProvider{}

	for {
		if dataClients, err := data(
			apiClient, apiContextTimeout, creativesByProvider.MapCreatives(),
		); err == nil {
			clients = dataClients
		}
		creativesByProvider = inspect(
			&http.Client{Timeout: time.Duration(tmtContextTimeout) * time.Second},
			tmtRequestTimeout,
			clients,
			creativesByProvider,
			providers,
		)
		sleep(timeout)
	}
}
