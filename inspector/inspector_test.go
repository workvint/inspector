package inspector

import (
	"bytes"
	"context"
	"errors"
	"io/ioutil"
	"net/http"
	"testing"

	"google.golang.org/grpc"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"

	"gitlab.some.systems/b/api/engine/node/pb"
	"gitlab.some.systems/b/customer"
	"gitlab.some.systems/o/taginspector/provider"
	"gitlab.some.systems/o/taginspector/provider/geoedge"
	"gitlab.some.systems/o/taginspector/provider/tmt"
)

var (
	errAPIClient = errors.New("apiClientError")
)

type InspectorSuite struct {
	suite.Suite

	prefix              provider.Prefix
	providers           MapProviders
	clients             MapClients
	creativesByProvider MapCreativesByProvider
	tmtRequestTimeout   int64

	registry *prometheus.Registry
}

func (s *InspectorSuite) SetupTest() {
	s.prefix = provider.NewPrefix("gobid")
	s.providers = NewMapProviders(s.prefix)
	s.clients = MapClients{
		"1": &pb.TagInspectorClient{
			TmtKey: "key1",
			Creatives: map[string]*customer.Creative{
				"1.1.1": &customer.Creative{
					AdId:        "1.1.1",
					HtmlContent: "htmlContent1",
				},
			},
		},
	}
	s.creativesByProvider = MapCreativesByProvider{
		"tmt": map[string][]string{
			"key1": []string{"1.1.1", "1.2.1"},
			"key2": []string{"2.1.1", "2.2.1"},
		},
	}

	s.registry = prometheus.NewPedanticRegistry()
	dataMetric.Reset()
	filterMetric.Reset()
	s.registry.MustRegister(dataMetric, filterMetric)
}

type StubAPIClient struct {
	response *pb.TagInspectorResponse
	err      error
}

func (s *StubAPIClient) Heartbeat(
	ctx context.Context,
	request *pb.Node,
	opts ...grpc.CallOption,
) (*pb.ConfigStore, error) {
	return nil, s.err
}

func (s *StubAPIClient) TagInspectorCheck(
	ctx context.Context,
	request *pb.TagInspectorRequest,
	opts ...grpc.CallOption,
) (*pb.TagInspectorResponse, error) {
	return s.response, s.err
}

func newAPIClient() *StubAPIClient {
	return &StubAPIClient{response: &pb.TagInspectorResponse{Clients: MapClients{}}}
}

func newAPIClientError() *StubAPIClient {
	return &StubAPIClient{err: errAPIClient}
}

func (s *InspectorSuite) testMetric(m map[string]string) {
	if m != nil {
		g, err := s.registry.Gather()
		s.Require().NoError(err)

		for _, label := range g[0].GetMetric()[0].GetLabel() {
			s.Equal(m[label.GetName()], label.GetValue())
		}
	}
}

func (s *InspectorSuite) TestData() {
	clients, err := data(newAPIClient(), 0, MapCreatives{})
	s.Equal(MapClients{}, clients)
	s.NoError(err)

	s.testMetric(map[string]string{
		"is_error": "false",
	})
}

func (s *InspectorSuite) TestDataError() {
	clients, err := data(newAPIClientError(), 0, MapCreatives{})
	s.Nil(clients)
	s.Equal(errAPIClient, err)

	s.testMetric(map[string]string{
		"is_error": "true",
	})
}

func testClient(client *pb.TagInspectorClient, id string) *pb.TagInspectorClient {
	id += ".1.1"
	client.Creatives = map[string]*customer.Creative{
		id: &customer.Creative{
			AdId:        id,
			HtmlContent: "htmlContent" + id,
		},
	}
	return client
}

func (s *InspectorSuite) TestDataTagsByProvider() {
	m := dataTagsByProvider(
		MapClients{
			"0":  testClient(&pb.TagInspectorClient{}, "0"),
			"1":  testClient(&pb.TagInspectorClient{TmtKey: "key1"}, "1"),
			"11": testClient(&pb.TagInspectorClient{TmtKey: "key1"}, "11"),
			"2":  testClient(&pb.TagInspectorClient{GeoedgeKey: "key1"}, "2"),
			"22": testClient(&pb.TagInspectorClient{GeoedgeKey: "key1"}, "22"),
		},
		s.providers,
	)
	s.Contains(m["tmt"]["key1"], tmt.NewTag(s.prefix, "1.1.1", "", "htmlContent1.1.1"))
	s.Contains(m["tmt"]["key1"], tmt.NewTag(s.prefix, "11.1.1", "", "htmlContent11.1.1"))
	s.Contains(m["geoedge"]["key1"], geoedge.NewProject(s.prefix, "2.1.1", "", "htmlContent2.1.1"))
	s.Contains(m["geoedge"]["key1"], geoedge.NewProject(s.prefix, "22.1.1", "", "htmlContent22.1.1"))
}

type StubClientInspect struct {
	responseSearch   *http.Response
	responseIncident *http.Response
	responsePause    *http.Response
	responseRemove   *http.Response
	responseAdd      *http.Response
	err              error
}

func (c *StubClientInspect) Do(request *http.Request) (r *http.Response, err error) {
	switch request.URL.Query().Get("action") {
	case "JSON_tag_import":
		r = c.responseAdd
	case "tag_pause":
		r = c.responsePause
	case "tag_remove":
		r = c.responseRemove
	case "search_tags":
		r = c.responseSearch
	case "tag_incident_summary":
		r = c.responseIncident
	default:
		r = newResponse("")
	}
	return r, c.err
}

func newResponse(body string) *http.Response {
	return &http.Response{
		Body:   ioutil.NopCloser(bytes.NewReader([]byte(body))),
		Header: http.Header{"Content-Type": []string{"text/html"}},
	}
}

func newClientInspect(
	bodySearch, bodyIncident, bodyPause, bodyRemove, bodyAdd string,
	err error,
) *StubClientInspect {
	return &StubClientInspect{
		responseSearch:   newResponse(bodySearch),
		responseIncident: newResponse(bodyIncident),
		responsePause:    newResponse(bodyPause),
		responseRemove:   newResponse(bodyRemove),
		responseAdd:      newResponse(bodyAdd),
		err:              err,
	}
}

type stubHttpTimeoutError struct{}

func (e stubHttpTimeoutError) Error() string {
	return "stubHttpTimeoutError"
}

func (e stubHttpTimeoutError) Timeout() bool {
	return true
}

func (e stubHttpTimeoutError) Temporary() bool {
	return false
}

func (s *InspectorSuite) TestInspect() {
	for _, tt := range []struct {
		clients                     MapClients
		searchResponse              string
		incidentResponse            string
		pauseResponse               string
		removeResponse              string
		addResponse                 string
		err                         error
		creativesByProviderExpected MapCreativesByProvider
		metricExpected              map[string]string
	}{
		{
			clients:                     s.clients,
			searchResponse:              `{}`,
			incidentResponse:            `{}`,
			pauseResponse:               `{}`,
			removeResponse:              `{}`,
			addResponse:                 `{"gobid_1.1.1":"Inserted one Record"}`,
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {"key1": []string{}}},
		},
		{
			clients:          s.clients,
			searchResponse:   `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"aHRtbENvbnRlbnQx","tagName":"gobid_1.1.1","lastScan":null,"pause":"f"}]}`,
			incidentResponse: `{}`,
			pauseResponse:    `{}`,
			removeResponse:   `{}`,
			addResponse:      `{}`,
			err:              errors.New("some error"),
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {}},
		},
		{
			clients:          s.clients,
			searchResponse:   `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"aHRtbENvbnRlbnQx","tagName":"gobid_1.1.1","lastScan":null,"pause":"f"}]}`,
			incidentResponse: `{}`,
			pauseResponse:    `{}`,
			removeResponse:   `{}`,
			addResponse:      `{}`,
			err:              stubHttpTimeoutError{},
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {"key1": []string{"1.1.1", "1.2.1"}}},
		},
		{
			clients:                     s.clients,
			searchResponse:              `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"aHRtbENvbnRlbnQx","tagName":"gobid_1.1.1","lastScan":null,"pause":"f"}]}`,
			incidentResponse:            `{}`,
			pauseResponse:               `{}`,
			removeResponse:              `{}`,
			addResponse:                 `{}`,
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {"key1": []string{}}},
			metricExpected:              map[string]string{"provider": "tmt", "creative": "1.1.1", "error": "FILTER_LAST_SCAN"},
		},
		{
			clients:                     s.clients,
			searchResponse:              `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"aHRtbENvbnRlbnQx","tagName":"gobid_1.1.1","lastScan":"1512348903","pause":"f"}]}`,
			incidentResponse:            `{}`,
			pauseResponse:               `{}`,
			removeResponse:              `{}`,
			addResponse:                 `{}`,
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {"key1": []string{"1.1.1"}}},
		},
		{
			clients:                     s.clients,
			searchResponse:              `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"aHRtbENvbnRlbnQx","tagName":"gobid_1.1.1","lastScan":"1512348903","pause":"f"}]}`,
			incidentResponse:            `Error: some error.`,
			pauseResponse:               `{}`,
			removeResponse:              `{}`,
			addResponse:                 `{}`,
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {}},
		},
		{
			clients:                     s.clients,
			searchResponse:              `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"aHRtbENvbnRlbnQx","tagName":"gobid_1.1.1","lastScan":"1512348903","pause":"f"}]}`,
			incidentResponse:            `{"count":1,"incidents":[{"adTagId":"gobid_1.1.1","tagName":"gobid_1.1.1","pause":"f"}]}`,
			pauseResponse:               `{"FAILED": ["gobid_1.1.1"]}`,
			removeResponse:              `{}`,
			addResponse:                 `{}`,
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {}},
		},
		{
			clients:                     s.clients,
			searchResponse:              `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"aHRtbENvbnRlbnQx","tagName":"gobid_1.1.1","lastScan":"1512348903","pause":"f"}]}`,
			incidentResponse:            `{"count":1,"incidents":[{"adTagId":"gobid_1.1.1","tagName":"gobid_1.1.1","pause":"f"}]}`,
			pauseResponse:               `{"SUCCESS": ["gobid_1.1.1"]}`,
			removeResponse:              `{}`,
			addResponse:                 `{}`,
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {"key1": []string{}}},
			metricExpected:              map[string]string{"provider": "tmt", "creative": "1.1.1", "error": "FILTER_INCIDENT"},
		},
		{
			clients:                     s.clients,
			searchResponse:              `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"aHRtbENvbnRlbnQx","tagName":"gobid_1.1.1","lastScan":"1512348903","pause":"t"}]}`,
			incidentResponse:            `{"count":1,"incidents":[{"adTagId":"gobid_1.1.1","tagName":"gobid_1.1.1","pause":"t"}]}`,
			pauseResponse:               `{}`,
			removeResponse:              `{}`,
			addResponse:                 `{}`,
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {"key1": []string{}}},
			metricExpected:              map[string]string{"provider": "tmt", "creative": "1.1.1", "error": "FILTER_INCIDENT"},
		},
		{
			clients:                     s.clients,
			searchResponse:              `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"b2xk","tagName":"gobid_1.1.1","lastScan":"1512348903","pause":"f"}]}`,
			incidentResponse:            `{}`,
			pauseResponse:               `{}`,
			removeResponse:              `{"FAILED": ["gobid_1.1.1"]}`,
			addResponse:                 `{}`,
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {}},
		},
		{
			clients:                     s.clients,
			searchResponse:              `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"b2xk","tagName":"gobid_1.1.1","lastScan":"1512348903","pause":"f"}]}`,
			incidentResponse:            `{}`,
			pauseResponse:               `{}`,
			removeResponse:              `{"SUCCESS": ["gobid_1.1.1"]}`,
			addResponse:                 `{}`,
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {}},
		},
		{
			clients:                     s.clients,
			searchResponse:              `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"b2xk","tagName":"gobid_1.1.1","lastScan":"1512348903","pause":"f"}]}`,
			incidentResponse:            `{}`,
			pauseResponse:               `{}`,
			removeResponse:              `{"SUCCESS": ["gobid_1.1.1"]}`,
			addResponse:                 `{"gobid_1.1.1":"Inserted one Record"}`,
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {"key1": []string{}}},
		},
		{
			clients: MapClients{"1": &pb.TagInspectorClient{
				TmtKey: "key1", Creatives: map[string]*customer.Creative{},
			}},
			searchResponse:              `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"b2xk","tagName":"gobid_1.1.1","lastScan":"1512348903","pause":"f"}]}`,
			incidentResponse:            `{}`,
			pauseResponse:               `{}`,
			removeResponse:              `{"FAILED": ["gobid_1.1.1"]}`,
			addResponse:                 `{}`,
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {}},
		},
		{
			clients: MapClients{"1": &pb.TagInspectorClient{
				TmtKey: "key1", Creatives: map[string]*customer.Creative{},
			}},
			searchResponse:              `{"count":1,"results":[{"adTagId":"gobid_1.1.1","creativeBase64":"b2xk","tagName":"gobid_1.1.1","lastScan":"1512348903","pause":"f"}]}`,
			incidentResponse:            `{}`,
			pauseResponse:               `{}`,
			removeResponse:              `{"SUCCESS": ["gobid_1.1.1"]}`,
			addResponse:                 `{}`,
			creativesByProviderExpected: MapCreativesByProvider{"tmt": {"key1": []string{}}},
		},
	} {
		filterMetric.Reset()

		creativesByProvider := inspect(
			newClientInspect(
				tt.searchResponse,
				tt.incidentResponse,
				tt.pauseResponse,
				tt.removeResponse,
				tt.addResponse,
				tt.err,
			),
			s.tmtRequestTimeout,
			tt.clients,
			s.creativesByProvider,
			s.providers,
		)
		s.Equal(tt.creativesByProviderExpected, creativesByProvider)

		s.testMetric(tt.metricExpected)
	}
}

func (s *InspectorSuite) TestInspectHttpClientError() {
	s.Equal(
		MapCreatives{},
		inspect(
			&StubClientInspect{err: errors.New("error")},
			s.tmtRequestTimeout,
			s.clients,
			s.creativesByProvider,
			s.providers,
		).MapCreatives(),
	)
}

func TestInspectorSuite(t *testing.T) {
	suite.Run(t, new(InspectorSuite))
}

func TestNewMapProviders(t *testing.T) {
	prefix := provider.NewPrefix("test_prefix")
	assert.Equal(
		t,
		MapProviders{
			"tmt":     tmt.NewTMT(prefix),
			"geoedge": geoedge.NewGeoEdge(prefix),
		},
		NewMapProviders(prefix),
	)
}

func TestFilters(t *testing.T) {
	tagFilters := []string{}
	for _, f := range filters(tmt.Tag{}, provider.TagList{}) {
		tagFilters = append(tagFilters, f.String())
	}
	assert.Equal(
		t,
		[]string{
			"Incident",
			"Pause",
			"LastScan",
		},
		tagFilters,
	)
}
